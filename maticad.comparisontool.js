﻿"use strict";

var Maticad = Maticad || {};
Maticad.Utils = Maticad.Utils || {};
Maticad.Utils.isMobileClient = function () {
    return typeof (window.orientation) !== 'undefined';
}

Maticad.ComparisonTool = (function () {
    var visualizerContainer;
    var visualizerCanvas;
    var compareImage;
    var ctHandle;
    var ctHandlePct = 0.5;

    var offsetX = 0;
    var offsetY = 0;
    var imgWidth = 0;
    var imgHeight = 0;

    var startEventNames;
    var moveEventNames;
    var endEventNames;
    var pointerPressed = false;

    var comparisonToolActive = false;

    var lastImgPath;

    var fixed_side;

    function showComparisonTool(img_path, visualizer_side) {
        var update = false;
        if (visualizer_side != fixed_side) {
            update = true;
        }
        
        fixed_side = visualizer_side;

        if (img_path != lastImgPath) {
            compareImage = document.getElementById("compare-image");
            compareImage.src = img_path;
            lastImgPath = img_path;
        }

        //compareImage = document.getElementById("compare-image");
        //compareImage.onload = function () {

        if (!comparisonToolActive) {
            var externalContainer = document.getElementById("external-container");
            externalContainer.classList.add("comparison-tool");

            visualizerContainer = document.getElementById("visualizer-container");
            visualizerContainer.classList.add("ct-container");
            imgWidth = window.innerWidth;
            imgHeight = window.innerHeight;

            visualizerCanvas = document.getElementById("visualizer-canvas");
            visualizerCanvas.classList.add("ct-before");

            compareImage.classList.remove("hidden");

            ctHandle = document.getElementById("ct-handle");
            ctHandle.classList.remove("hidden");

            ctHandlePct = 0.5;
            adjustHandle(ctHandlePct);

            addMouseTouchEvent();

            comparisonToolActive = true;            
        }
        
        if (update) {
            adjustHandle(ctHandlePct);
        }
                
        //};
        //compareImage.onerror = function () {
        //    console.log("compareImage onloadError");
        //};
        //compareImage.src = img_path;
    }

    function hideComparisonTool() {
        var externalContainer = document.getElementById("external-container");
        externalContainer.classList.remove("comparison-tool");

        visualizerContainer.classList.remove("ct-container");

        visualizerCanvas.classList.remove("ct-before");
        //visualizerCanvas.style.removeProperty('clip');

        compareImage.classList.add("hidden");
        compareImage.style.removeProperty('clip');

        ctHandle.classList.add("hidden");

        removeMouseTouchEvent();

        compareImage.src = "#";

        comparisonToolActive = false;
    }

    function resize() {
        imgWidth = visualizerContainer.width;
        imgHeight = visualizerContainer.height;

        adjustHandle(ctHandlePct);
    }

    var calcOffset = function (dimensionPct) {
        //var w = visualizerContainer.width;
        //var h = visualizerContainer.height;
        return {
            w: imgWidth + "px", //w
            h: imgHeight + "px", //h
            cw: (dimensionPct * imgWidth) + "px", //w
            ch: (dimensionPct * imgHeight) + "px" //h
        };
    };

    var adjustHandle = function (pct) {
        var offset = calcOffset(pct);
        console.log('abjusthandle', offset);
        ctHandle.style.left = offset.cw;

        adjustContainer(offset);
    }

    var adjustContainer = function (offset) {
        //ATTENTION: clip not working with safari webgl canvas
        //visualizerCanvas.style.clip = "rect(0," + offset.cw + "," + offset.h + ",0)";

        if (fixed_side == "left") {
            compareImage.style.clip = "rect(0," + offset.w + "," + offset.h + "," + offset.cw + ")";
        }
        else if (fixed_side == "right") {
            compareImage.style.clip = "rect(0," + offset.cw + "," + offset.h + ", 0)";
        }

        visualizerContainer.style.height = offset.h;
    };

    function addMouseTouchEvent() {
        startEventNames = Maticad.Utils.isMobileClient() ? "touchstart" : "mousedown";
        ctHandle.addEventListener(startEventNames, startEvent);

        moveEventNames = Maticad.Utils.isMobileClient() ? "touchmove" : "mousemove";
        ctHandle.addEventListener(moveEventNames, moveEvent);
        visualizerContainer.addEventListener(moveEventNames, moveEvent);

        endEventNames = Maticad.Utils.isMobileClient() ? "touchcancel  touchend" : "mouseup";
        ctHandle.addEventListener(endEventNames, endEvent);
    }

    function removeMouseTouchEvent() {
        ctHandle.removeEventListener(startEventNames, startEvent);

        ctHandle.removeEventListener(moveEventNames, moveEvent);
        visualizerContainer.removeEventListener(moveEventNames, moveEvent);

        ctHandle.removeEventListener(endEventNames, endEvent);
    }

    function startEvent(e) {
        //e.preventDefault();
        // console.log("cthandle: touchstart/mousedown");

        pointerPressed = (e.which == 1) || (e.touches.length == 1);
        console.log("cthandle: touchstart/mousedown", pointerPressed);

        //if (((e.distX > e.distY && e.distX < -e.distY) || (e.distX < e.distY && e.distX > -e.distY))) {
        //    e.preventDefault();
        //    console.log("cthandle: touchstart/mousedown -> e.preventDefault()");
        //}
    }

    function moveEvent(e) {
        if (pointerPressed) {
            //e.preventDefault();
            //console.log("cthandle: touchmove/mousemove");

            ctHandlePct = (e.pageX - offsetX) / imgWidth;
            if (ctHandlePct < 0) {
                ctHandlePct = 0;
            }
            if (ctHandlePct > 1) {
                ctHandlePct = 1;
            }
            console.log('mouse move');
            adjustHandle(ctHandlePct);
            
        }

    }

    function endEvent(e) {
        //e.preventDefault();
        //if (pointerPressed) {
        //console.log("cthandle: touchcancel  touchend/mouseup");
        //}
        pointerPressed = false;

        //Maticad.Messaging.visualizerSplitterPosition(ctHandlePct);
    }


    return {
        showComparisonTool: showComparisonTool,
        hideComparisonTool: hideComparisonTool,
        resize: resize
    };

})();